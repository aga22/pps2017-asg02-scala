name := "pps2017-asg02-scala"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

libraryDependencies += "it.unibo.alice.tuprolog" % "tuprolog" % "3.2.1"