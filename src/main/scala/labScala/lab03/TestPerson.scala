package labScala.lab03

sealed trait Person

object Person {

  case class Student(name: String, year: Int) extends Person

  case class Teacher(name: String, course: String) extends Person

  def name(p: Person): String = p match {
    case Student(n, _) => n
    case Teacher(n, _) => n
  }

  def personToString(p: Person): String = p match {
    case Teacher(n, m) => n + ": [" + m + "]"
    case Student(n, m) => n + ": " + m
  }

  import List._

  def courses(l: List[Person]): List[String] = map(filter(l)(_.isInstanceOf[Teacher]))(t => t.asInstanceOf[Teacher].course)
}

object TestPerson extends App {

  import Person._
  import List._

  personToString(Teacher("mirko", "PPS"))
  personToString(Student("mario", 2016))
  println(personToString(Teacher("mirko", "PPS")))
  println(personToString(Student("mario", 2016)))

  val teacher1 = Teacher("mirko1", "PPS1")
  val teacher2 = Teacher("mirko2", "PPS2")
  val teacher3 = Teacher("mirko3", "PPS3")
  val teacher4 = Teacher("mirko4", "PPS4")
  val student1 = Student("mario1", 2016)
  val student2 = Student("mario2", 2017)
  val list: List[Person] = Cons(teacher1, Cons(teacher2, Cons(teacher3, Cons(teacher4, Cons(student1, Cons(student2, Nil()))))))

  println(courses(list))
}
