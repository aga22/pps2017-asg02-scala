package labScala.lab03

import List._

sealed trait List[E]

object List {

  case class Cons[E](head: E, tail: List[E]) extends List[E]

  case class Nil[E]() extends List[E]

  def length[E](l: List[E]): Int = l match {
    case Cons(_, t) => 1 + length(t)
    case _ => 0
  }

  def sum(l: List[Int]): Int = l match {
    case Cons(h, t) => h + sum(t)
    case _ => 0
  }

  def append[A <: C, B <: C, C](l1: List[A], l2: List[B]): List[C] = (l1, l2) match {
    case (Cons(h, t), list2) => Cons[C](h, append(t, list2))
    case (list1, Cons(h, t)) => Cons[C](h, append(list1, t))
    case _ => Nil()
  }

  def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
    case (list, num) if num <= 0 => list
    case (Cons(h, t), num) if num < length(Cons(h, t)) => drop(t, num - 1)
    case (list, num) if num >= length(list) => Nil()
  }

  def map[A, B](l: List[A])(f: A => B): List[B] = l match {
    case list if length(list) < 1 => Nil()
    case Cons(h, t) => Cons[B](f(h), map(t)(f))
  }

  def filter[A](l: List[A])(f: A => Boolean): List[A] = l match {
    case list if length(list) < 1 => Nil()
    case Cons(h, t) if f(h) => Cons(h, filter(t)(f))
    case Cons(h, t) if !f(h) => drop(Cons(h, filter(t)(f)), 1)
  }
}

object Test extends App {

  val list = Cons(10, Cons(20, Cons(30, Nil())))

  println(length(list)) // 2
  println(sum(list)) // 60
  println(drop(list, 2))
  println(filter(list)(_ > 15)) // Cons(20,Nil())
  println(filter(Cons("a", Cons("bb", Cons("ccc", Nil()))))(_.length <= 2)) // Cons("a",Cons("bb", Nil())))
  println(map(Cons(10, Cons(20, Nil())))(":" + _ + ":")) // Cons(":10:", Cons(":20:", Nil())))
}
