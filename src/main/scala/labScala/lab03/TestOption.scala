package labScala.lab03

import List._

sealed trait Option[A]

object Option {

  case class None[A]() extends Option[A]

  case class Some[A](a: A) extends Option[A]

  def isEmpty[A](opt: Option[A]): Boolean = opt != None()

  def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
    case Some(a) => a
    case _ => orElse
  }

  def getOrNone[A](opt: Option[A]): A = opt match {
    case Some(a) => a
    case None() => throw new Exception
  }

  def max(l: List[Int]): Option[Int] = l match {
    case Cons(h, t) if h > getOrElse(max(t), 0) => Some(h)
    case Cons(h, t) if h <= getOrElse(max(t), 0) => max(t)
    case _ => None()
  }

  def filter[A](o: Option[A])(f: A => Boolean): Option[A] = o match {
    case t if f(getOrNone(t)) => t
    case _ => None()
  }

  def map[A, B](o: Option[A])(f: A => B): Option[B] = o match {
    case None() => None()
    case t => Some[B](f(getOrNone(t)))
  }
}


object TestOption extends App {

  import Option._
  import List.Cons

  println(max(Cons(10, Cons(25, Cons(21, Nil()))))) // Some(25)
  println(max(Cons(10, Cons(25, Cons(21, Nil()))))) // Some(25)
  println(filter(Some(5))(_ > 2)) // Some(5)
  println(filter(Some(5))(_ > 8)) // None
  println(map(Some(5))(_ > 2)) // Some(true)
  println(map(None[Int]())(_ > 2)) // None
}
