package labScala.lab05

import scala.util.Random
import scala.util.control.Breaks

object TicTacToe extends App {

  sealed trait Player {
    def other: Player = this match {
      case X => O;
      case _ => X
    }

    override def toString: String = this match {
      case X => "X";
      case _ => "O"
    }
  }

  case object X extends Player

  case object O extends Player

  case class Mark(x: Double, y: Double, player: Player)

  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Double, y: Double): Option[Player] = board find (i => i.x.equals(x) && i.y.equals(y)) map (i => i.player)

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    var boards: Seq[Board] = Seq[Board]()
    //((2 to 0 by -1) zip (2 to 0 by -1)).filterNot(a => board.exists(m => m.x == a._1 && m.y == a._2)).foreach(a => boards = boards.+:(board.::(Mark(a._1, a._2, player))))
    for (x <- 2 to 0 by -1; y <- 2 to 0 by -1) if (!board.exists(m => m.x == x && m.y == y)) boards = boards.+:(board.::(Mark(x, y, player)))
    boards
  }

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = {
    var game: Stream[Game] = Stream[Game]()
    var board: List[Mark] = List[Mark]()
    var boards: List[Board] = List[Board]()
    var marks: List[Mark] = List[Mark]()
    val rnd = new Random()
    var currentPlayer = player
    val loop = new Breaks

    for (x <- 0 to 2; y <- 0 to 2) board = board :+ Mark(x, y, player)

    boards = List[Mark]() :: boards

    loop.breakable {
      for (_ <- 0 until moves) {
        val randomMark = board(rnd.nextInt(board.size))
        marks = Mark(randomMark.x, randomMark.y, currentPlayer) :: marks
        board = board.filterNot(elem => elem == randomMark)
        boards = marks :: boards
        if (checkWin(marks, currentPlayer)) {
          println("GAME OVER! PLAYER " + currentPlayer.toString + " WON THE GAME!")
          game = game :+ boards
          loop.break()
        }
        if (board.isEmpty) {
          println("GAME OVER! NOBODY WON THE GAME!")
          game = game :+ boards
          loop.break()
        }
        if (currentPlayer.equals(X)) currentPlayer = O else currentPlayer = X
      }
    }

    if (game.isEmpty) {
      game = game :+ boards
      println("GAME OVER! NOBODY WON THE GAME!")
    }
    game
  }

  def checkWin(currentBoard: List[Mark], player: Player): Boolean = this match {
    case _ if (currentBoard.count(mark => mark.x == 0 && mark.player.equals(player)) == 3) ||
      (currentBoard.count(mark => mark.x == 1 && mark.player.equals(player)) == 3) ||
      (currentBoard.count(mark => mark.x == 2 && mark.player.equals(player)) == 3) ||
      (currentBoard.count(mark => mark.y == 0 && mark.player.equals(player)) == 3) ||
      (currentBoard.count(mark => mark.y == 1 && mark.player.equals(player)) == 3) ||
      (currentBoard.count(mark => mark.y == 2 && mark.player.equals(player)) == 3) ||
      (currentBoard.contains(Mark(0, 0, player)) && currentBoard.contains(Mark(1, 1, player))
        && currentBoard.contains(Mark(2, 2, player))) ||
      (currentBoard.contains(Mark(0, 2, player)) && currentBoard.contains(Mark(1, 1, player))
        && currentBoard.contains(Mark(2, 0, player))) => true
    case _ => false
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ".")
      if (x == 2) {
        print(" ")
        if (board == game.head) println()
      }
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0, 0, X)), 0, 0)) // Some(X)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 0, 1)) // Some(O)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 1, 1)) // None

  println("--First Board--")
  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(), X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  println("--Second Board--")
  printBoards(placeAnyMark(List(Mark(0, 0, O)), X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...
  println("--Third Board--")
  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O, 4) foreach { g => printBoards(g); println() }
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
}