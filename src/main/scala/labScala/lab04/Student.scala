package labScala.lab04

trait Student {
  def name: String

  def year: Int

  def enrolling(course: Course*): Unit // the student participates to a Course
  def courses: List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?
}

trait Course {
  def name: String

  def teacher: String
}

case class CourseImpl(name: String, teacher: String) extends Course

case class StudentImpl(name: String, year: Int = 2017) extends Student {

  private var courseSet: List[Course] = List[Course]()

  override def enrolling(course: Course*): Unit = {
    for (i <- course.length - 1 to 0 by -1) courseSet = course(i) :: courseSet
  }

  override def courses: List[String] = courseSet.map(i => i.name)

  override def hasTeacher(teacher: String): Boolean = courseSet.map(i => i.teacher == teacher).get(0).get
}

object Student {
  def apply(name: String, year: Int = 2017): Student = StudentImpl(name, year)
}

object Course {
  def apply(name: String, teacher: String): Course = CourseImpl(name, teacher)
}

object Try extends App {
  val cPPS = Course("PPS", "Viroli")
  val cPCD = Course("PCD", "Ricci")
  val cSDR = Course("SDR", "D'Angelo")
  val s1 = Student("mario", 2015)
  val s2 = Student("gino", 2016)
  val s3 = Student("rino") //defaults to 2017

  s1.enrolling(cPCD, cPPS, cSDR)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)
  println(s1.courses, s2.courses, s3.courses) // (Cons(PCD,Cons(PPS,Nil())),Cons(PPS,Nil()),Cons(SDR,Cons(PCD,Cons(PPS,Nil()))))
  println(s2.hasTeacher("Viroli")) // true
  println(s3.year)
}

/** Hints:
  * - simply implement Course, e.g. with a case class
  * - implement Student with a StudentImpl keeping a private Set of courses
  * - try to implement in StudentImpl method courses with map
  * - try to implement in StudentImpl method hasTeacher with map and find
  * - check that the two println above work correctly
  * - refactor the code so that method enrolling accepts a variable argument Course*
  */
