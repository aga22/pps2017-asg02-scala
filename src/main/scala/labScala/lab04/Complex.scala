package labScala.lab04

trait Complex {
  def re: Double

  def im: Double

  def +(c: Complex): Complex

  def *(c: Complex): Complex

  override def toString: String = re + " " + im + " "
}

object Complex {
  def apply(re: Double, im: Double): Complex = ComplexImpl(re, im)
}

case class ComplexImpl(re: Double, im: Double) extends Complex {
  override def +(c: Complex): Complex = ComplexImpl(re + c.re, im + c.im)

  override def *(c: Complex): Complex = ComplexImpl(re * c.re - im * c.im, re * c.im + im * c.re)
}

object TryComplex extends App {
  val a = Array(Complex(10, 20), Complex(1, 1), Complex(7, 0))
  val c = a(0) + a(1) + a(2)
  println(c, c.re, c.im) // (ComplexImpl(18.0,21.0),18.0,21.0)
  val c2 = a(0) * a(1)
  println(c2, c2.re, c2.im) // (ComplexImpl(-10.0,30.0),-10.0,30.0)
  var b: Complex = ComplexImpl(14, 2)
  val e: Complex = ComplexImpl(14, 2)
  print(b.equals(e))
}
