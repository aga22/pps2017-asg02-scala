package labScala.lab09

import org.scalatest._

class SetSpec extends FlatSpec {
  var fruit: Set[String] = Set.empty
  override def withFixture(test: NoArgTest): Outcome = { // Define a shared fixture
    // Shared setup (run at beginning of each test)
    fruit = Set("apple", "orange", "peach", "banana")
    try test()
    finally {
      // Shared cleanup (run at end of each test)
    }
  }

  // Define the first test for a subject, in this case: "An empty Set"
  "An empty Set" should "have size 0" in { // Name the subject, write 'should', then the rest of the test name
    assert(fruit.isEmpty) // (Can use 'must' or 'can' instead of 'should')
  }
  it should "produce NoSuchElementException when head is invoked" in { // Define another test for the same
    intercept[NoSuchElementException] { // subject with 'it'
      Set.empty.head
    }
  }
  ignore should "be empty" in { // To ignore a test, change 'it' to 'ignore'...
    assert(Set.empty.isEmpty)
  }
  it should "not be non-empty" ignore { // ...or change 'in' to 'ignore'
    assert(Set.empty.isEmpty)
  }
  "A non-empty Set" should "have the correct size" in { // Describe another subject
    assert(Set(1, 2, 3).size == 3)
  }
  // 'it' now refers to 'A non-empty Set'
  it should "return a contained value when head is invoked" is pending // Define a pending test
  import tagobjects.Slow

  it should "be non-empty" taggedAs Slow in { // Tag a test
    assert(Set(1, 2, 3).nonEmpty)
  }
}

// Can also pass fixtures into tests with fixture.FlatSpec
class StringSpec extends fixture.FlatSpec {
  type FixtureParam = String // Define the type of the passed fixture object
  override def withFixture(test: OneArgTest): Outcome = {
    // Shared setup (run before each test), including...
    val fixture = "a fixture object" // ...creating a fixture object
    try test(fixture) // Pass the fixture into the test
    finally {
      // Shared cleanup (run at end of each test)
    }
  }

  "The passed fixture" can "be used in the test" in { s => // Fixture passed in as s
    assert(s == "a fixture object")
  }
}

@DoNotDiscover // Disable discovery of a test class
class InvisibleSpec extends FlatSpec {
  /*code omitted*/
}

@Ignore // Ignore all tests in a test class
class IgnoredSpec extends FlatSpec {
  /*code omitted*/
}

import tags.Slow

@Slow // Mark all tests in a test class with a tag
class SlowSpec extends FlatSpec {
  /*code omitted*/
}